package ru.tsc.gavran.tm;

import ru.tsc.gavran.tm.bootstrap.Bootstrap;

public class Application {

    public static void main(String[] args) {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.start(args);
    }

}