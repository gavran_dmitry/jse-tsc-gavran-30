package ru.tsc.gavran.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractTaskCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.TaskNotFoundException;
import ru.tsc.gavran.tm.model.Task;
import ru.tsc.gavran.tm.util.TerminalUtil;

import java.util.Arrays;

public class TaskChangeStatusByIndexCommand extends AbstractTaskCommand {

    @NotNull
    @Override
    public String name() {
        return "task-change-status-by-index";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Change task status by index.";
    }

    @Override
    public void execute() {
        @NotNull final String userId = serviceLocator.getAuthService().getCurrentUserId();
        System.out.println("ENTER INDEX:");
        @Nullable final Integer index = TerminalUtil.nextNumber() - 1;
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        @Nullable final String statusValue = TerminalUtil.nextLine();
        @NotNull final Status status = Status.valueOf(statusValue);
        @Nullable final Task task = serviceLocator.getTaskService().changeStatusByIndex(userId, index, status);
        if (task == null) throw new TaskNotFoundException();
        serviceLocator.getTaskService().changeStatusByIndex(userId, index, status);
        System.out.println("[OK]");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return Role.values();
    }

}