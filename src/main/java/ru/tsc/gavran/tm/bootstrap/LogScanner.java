package ru.tsc.gavran.tm.bootstrap;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class LogScanner {

    @NotNull
    private static final String PATH = "./";

    public void init() {
        ArrayList<String> list = new ArrayList<>();
        list.add("commands.txt");
        list.add("commands.txt.lck");
        list.add("messages.txt");
        list.add("messages.txt.lck");
        list.add("errors.txt");
        list.add("errors.txt.lck");
        @NotNull final File file = new File(PATH);
        Arrays.stream(file.listFiles())
                .filter(o -> o.isFile() && list.contains(o.getName()))
                .forEach(o -> {
                    o.delete();
                });
    }
}
