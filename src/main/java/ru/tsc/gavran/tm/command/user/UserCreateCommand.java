package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserCreateCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-create";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Nullable
    @Override
    public String description() {
        return "Create new user.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        System.out.println("ENTER PASSWORD: ");
        @Nullable final String password = TerminalUtil.nextLine();
        System.out.println("ENTER EMAIL: ");
        @Nullable final String email = TerminalUtil.nextLine();
        serviceLocator.getUserService().create(login, password, email);
    }

}