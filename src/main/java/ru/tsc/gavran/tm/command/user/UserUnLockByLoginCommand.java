package ru.tsc.gavran.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractUserCommand;
import ru.tsc.gavran.tm.enumerated.Role;
import ru.tsc.gavran.tm.exception.system.AccessDeniedException;
import ru.tsc.gavran.tm.util.TerminalUtil;

public class UserUnLockByLoginCommand extends AbstractUserCommand {

    @NotNull
    @Override
    public String name() {
        return "user-unlock-by-login";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "Unlock user login.";
    }

    @Override
    public void execute() {
        System.out.println("ENTER USER LOGIN: ");
        @Nullable final String login = TerminalUtil.nextLine();
        @Nullable final String id = serviceLocator.getUserService().findById(login).getId();
        @NotNull final String currentUserId = serviceLocator.getAuthService().getCurrentUserId();
        if (id.equals(currentUserId)) throw new AccessDeniedException();
        serviceLocator.getUserService().unlockUserByLogin(login);
        System.out.println("USER UNLOCK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}