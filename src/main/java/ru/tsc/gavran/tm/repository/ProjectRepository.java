package ru.tsc.gavran.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.repository.IProjectRepository;
import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.gavran.tm.exception.system.ProcessException;
import ru.tsc.gavran.tm.model.Project;

import java.util.Optional;

public final class ProjectRepository extends AbstractOwnerRepository<Project> implements IProjectRepository {

    @NotNull
    @Override
    public Project findByName(@NotNull final String userId, @NotNull final String name) {
        return findAll(userId).stream()
                .filter(p -> p.getName().equals(name))
                .findFirst()
                .orElseThrow(ProjectNotFoundException::new);
    }

    @NotNull
    @Override
    public Project removeByName(@NotNull final String userId, @NotNull final String name) {
        @NotNull final Optional<Project> project = Optional.ofNullable(findByName(userId, name));
        project.ifPresent(this::remove);
        return project.orElseThrow(ProcessException::new);
    }

    @Nullable
    @Override
    public Project startById(@NotNull final String userId, @NotNull final String id) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project startByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project startByIndex(@NotNull final String userId, @NotNull final int index) {
        final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.IN_PROGRESS);
        return project;
    }

    @Nullable
    @Override
    public Project finishById(@NotNull final String userId, @NotNull final String id) {
        final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project finishByName(@NotNull final String userId, @NotNull final String name) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project finishByIndex(@NotNull final String userId, @NotNull final int index) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(Status.COMPLETED);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusById(@NotNull final String userId, @NotNull final String id, @NotNull final Status status) {
        @Nullable final Project project = findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByName(@NotNull final String userId, @NotNull String name, @NotNull Status status) {
        @Nullable final Project project = findByName(userId, name);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

    @Nullable
    @Override
    public Project changeStatusByIndex(@NotNull final String userId, @NotNull int index, @NotNull Status status) {
        @Nullable final Project project = findByIndex(userId, index);
        if (project == null) return null;
        project.setStatus(status);
        return project;
    }

}