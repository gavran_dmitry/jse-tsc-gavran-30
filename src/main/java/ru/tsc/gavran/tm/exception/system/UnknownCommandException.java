package ru.tsc.gavran.tm.exception.system;

import ru.tsc.gavran.tm.constant.TerminalConst;
import ru.tsc.gavran.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException() {
        super("Incorrect command! Please use `" + TerminalConst.HELP + "` for display commands.");
    }

}