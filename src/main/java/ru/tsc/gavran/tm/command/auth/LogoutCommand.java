package ru.tsc.gavran.tm.command.auth;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.command.AbstractCommand;

public class LogoutCommand extends AbstractCommand {

    @NotNull
    @Override
    public String name() {
        return "logout";
    }

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String description() {
        return "User logout from system.";
    }

    @Override
    public void execute() {
        serviceLocator.getAuthService().logout();
    }

}